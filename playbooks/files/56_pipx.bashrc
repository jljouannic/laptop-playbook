# pipx needs ~/.local/bin to be in PATH
case ":${PATH}:" in
    *:"$HOME/.local/bin":*)
        ;;
    *)
        export PATH="$HOME/.local/bin:$PATH"
        ;;
esac

eval "$(register-python-argcomplete pipx)"

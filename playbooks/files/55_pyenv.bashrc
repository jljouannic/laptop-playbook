export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
# Inspired by https://github.com/pyenv/pyenv/issues/264#issuecomment-654813922
[ -z "$PS1" ] && return
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

if [ -e "$HOME/.pyenv/.pyenvrc" ]; then
  source "$HOME/.pyenv/.pyenvrc"
  if [ -e "$HOME/.pyenv/completions/pyenv.bash" ]; then
    source "$HOME/.pyenv/completions/pyenv.bash"
  fi
fi

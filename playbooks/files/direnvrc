#!/usr/bin/env bash

# Usage: use nvm [<version>]
#
# Uses NVM to load the specified NodeJS version into the environment.
#
# If no version is passed, it will look at the '.nvmrc' files in the current directory
# if it exist.
#
# If the version is not installed, 'nvm install' is called to install it.
#
use_nvm() {
  local version=${1:-}

  if [[ -z ${NVM_DIR:-} || ! -d $NVM_DIR ]]; then
    log_error "nvm must be installed prior to using this command"
    return 1
  fi

  # shellcheck source=/dev/null
  \. "$NVM_DIR/nvm.sh"

  if [[ -z $version && -f .nvmrc ]]; then
    version=$(<.nvmrc)
  fi

  if [[ -z $version ]]; then
    log_error "I do not know which NodeJS version to load because one has not been specified!"
    log_error "You must specify a version number in .envrc file (e.g. 'use nvm v14') or in a .nvmrc file (e.g. 'v14')"
    return 1
  fi

  if ! nvm use "$version"; then
    nvm install "$version"
  fi

  log_status "Successfully loaded NodeJS $(node --version) via nvm"
}
